#include <iostream>

struct Shape
{
    virtual int calculateArea() = 0;
    virtual ~Shape(){}
};

struct Circle : Shape
{
    Circle(int radius) : radius(radius){}
    int calculateArea() override
    {
        return 3 * radius * radius;
    }
private:
    int radius;
};

struct Square : Shape
{
    Square(int a) :a(a) {}
    int calculateArea() override
    {
        return a * a;
    }
private:
    int a;
};

int main()
{
    Circle circle{5};
    Square square{5};
    std::cout << circle.calculateArea() << std::endl;
    std::cout << square.calculateArea() << std::endl;

    Shape& shape = circle;
    std::cout << shape.calculateArea() << std::endl;
}
