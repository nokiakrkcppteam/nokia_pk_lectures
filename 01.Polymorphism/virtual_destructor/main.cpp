#include <iostream>
#include <memory>

struct Base
{
    virtual void foo()
    {
        std::cout << "Base::foo" << std::endl;
    }
    virtual ~Base(){}
};

struct Embedded
{
    Embedded()
    {
        std::cout << "Embedded" << std::endl;
    }
    ~Embedded()
    {
        std::cout << "~Embedded" << std::endl;
    }
};

struct Derived: Base
{
    std::shared_ptr<Embedded> embedded = std::make_shared<Embedded>();
    void foo()
    {
        std::cout << "Derived::foo" << std::endl;
    }
};

int main()
{
    std::unique_ptr<Base> base = std::make_unique<Derived>();
    base->foo();
}
