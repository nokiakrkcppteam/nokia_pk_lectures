#include <iostream>

void foo(char)
{
    std::cout<< "foo(char)" << std::endl;
}

int foo(short)
{
    std::cout << "foo(short)" << std::endl;
    return 7;
}

int foo(short, double)
{
    std::cout << "foo(short, double)" << std::endl;
    return 7;
}

struct A
{
   void foo()
   {
       std::cout << "A::foo()" << std::endl;
   }
   void foo(int)
   {
       std::cout << "A::foo(int)" << std::endl;
   }
};

struct B : A
{
    B(int) {}
    using A::foo;
    void foo(double)
    {
        std::cout << "B::foo(double)" << std::endl;
    }
    void foo(double) const
    {
        std::cout << "B::foo(double) const" << std::endl;
    }
};



int operator+(const B&, const A&)
{
    return 5;
}

int operator+(const A&, const B&)
{
    return 7;
}

int main()
{
    int a = 0;
    short b = 4;
    foo(static_cast<char>(a));
    foo(b);
    char c = 9;
    foo(c);
    foo(a,b);

    A as{};
    as.foo();
    as.foo(3);

    B bs{1};
    bs.foo(3);

    bs.foo();

    bs.foo(2.4);
    const B cbs{3};
    cbs.foo(4.5);
    cbs.foo();
}
