#include <iostream>
#include <thread>


static int global_counter = 0;

void f(int)
{
    for (int i = 0; i < 50000; i++)
    {
        std::cout << "Current counter " << ++global_counter << " thread " << std::this_thread::get_id() << std::endl;
    }
}

int main()
{
    std::thread t1(f, 2);
    std::thread t2(f, 3);
    std::thread t3(f, 4);
    f(1);
    t1.join();
    t2.join();
    t3.join();
}
