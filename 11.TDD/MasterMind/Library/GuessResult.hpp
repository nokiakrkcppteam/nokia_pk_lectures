#pragma once

struct GuessResult
{
    unsigned exactMatches;
    unsigned partialMatches;
    bool success;
};
