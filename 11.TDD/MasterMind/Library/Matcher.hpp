#pragma once

#include "GuessResult.hpp"
#include "Pattern.hpp"
#include "Guess.hpp"

namespace MasterMind
{

class Matcher
{
public:
    GuessResult match(const Pattern& pattern, const Guess& guess);
};

}
