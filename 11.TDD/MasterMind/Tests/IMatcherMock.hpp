#include "IMatcher.hpp"

#include <gmock/gmock.h>

namespace MasterMind
{

class IMatcherMock : public IMatcher
{
public:
    MOCK_METHOD2(match, GuessResult(const Pattern&, const Guess&));
};

}
